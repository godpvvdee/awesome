import client from "./client";
import imageUrlBuilder from "@sanity/image-url";

const builder = imageUrlBuilder(client);

export const urlFor = (source) => {
	return builder.image(source);
};
export const getAllProduct = async () => {
	return await client.fetch(
		`*[_type == 'product' ]{name, bio, "slug": slug.current, 'image': image.asset->url }`	
		)
}
export const getEngiinElectron = async () => {
	return await client.fetch(
		`*[_type == 'engiinElectronTooluur' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getEngiinBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'engiinElectronTooluur' && slug.current==$slug ]{name, bio, "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
export const getOlonTaript = async () => {
	return await client.fetch(
		`*[_type == 'olontaript' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getOlonTariptBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'olontaript' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
export const getCollector = async () => {
	return await client.fetch(
		`*[_type == 'collector' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getCollectorBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'collector' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
export const getConcentrator = async () => {
	return await client.fetch(
		`*[_type == 'concentrator' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getConcentratorBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'concentrator' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// ухаалаг ус
export const getUhaalagUs = async () => {
	return await client.fetch(
		`*[_type == 'usniiTooluurUhaalag' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getUhaalagUsBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'usniiTooluurUhaalag' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// энгийн ус
export const getEngiinUs = async () => {
	return await client.fetch(
		`*[_type == 'usniiTooluurEngiin' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getEngiiinUsBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'usniiTooluurEngiin' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// manometr 
export const getManometr = async () => {
	return await client.fetch(
		`*[_type == 'manometr' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getManometrBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'manometr' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// main 
export const getMain = async () => {
	return await client.fetch(
		`*[_type == 'mainVideo' ]{ 'youtube':youtube.url}`	
		)
}
// end
// manometr 
export const getautoMashiniiJin = async () => {
	return await client.fetch(
		`*[_type == 'autoMashiniiJin' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getautoMashiniiJinBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'autoMashiniiJin' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// ажлын туршлага 
export const getTurshlaga = async () => {
	return await client.fetch(
		`*[_type == 'turshlaga' ]{name, bio, "slug": slug.current, 'image': image.asset->url }`
		)
}
export const getTurshlagaBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'turshlaga' && slug.current==$slug ]{name,  "slug": slug.current, 'image': image.asset->url }`
		,{slug}
		);
	
}
// end
// manometr 
export const getDaralt = async () => {
	return await client.fetch(
		`*[_type == 'daraltiinProcess' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getDaraltBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'daraltiinProcess' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// tamadum 
export const getTamadum = async () => {
	return await client.fetch(
		`*[_type == 'tamadum' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getTamadumBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'tamadum' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// ухаалаг гарц 
export const getUhaalagGarts = async () => {
	return await client.fetch(
		`*[_type == 'uhaalagGarts' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getUhaalagGartsBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'uhaalagGarts' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// ухаалаг гарц 
export const getTsahilgaanLab = async () => {
	return await client.fetch(
		`*[_type == 'tsahilgaanTooluurLab' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getTsahilgaanLabBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'tsahilgaanTooluurLab' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// uslab 
export const getUslab = async () => {
	return await client.fetch(
		`*[_type == 'usniiTooluuriinLab' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getUslabBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'usniiTooluuriinLab' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// jin zaswar 
export const getJinZaswar = async () => {
	return await client.fetch(
		`*[_type == 'jingiinZaswar' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getJinZaswarBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'jingiinZaswar' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// jin zaswar 
export const getBusad = async () => {
	return await client.fetch(
		`*[_type == 'busadZaswar' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getBusadBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'busadZaswar' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// analytic 
export const getAnalytic = async () => {
	return await client.fetch(
		`*[_type == 'analytic' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getAnalyticBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'analytic' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// ondor 
export const getOndor = async () => {
	return await client.fetch(
		`*[_type == 'ondorNariiwchlal' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getOndorBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'ondorNariiwchlal' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// hudaldaa 
export const getHudaldaaJin = async () => {
	return await client.fetch(
		`*[_type == 'hudaldaaniiJin' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getHudaldaaJinBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'hudaldaaniiJin' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// hudaldaa 
export const getTawtsan = async () => {
	return await client.fetch(
		`*[_type == 'tawtsant' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getTawtsanBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'tawtsant' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// hudaldaa 
export const getLombard = async () => {
	return await client.fetch(
		`*[_type == 'lombard' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getLombardBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'lombard' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// hudaldaa 
export const getAhui = async () => {
	return await client.fetch(
		`*[_type == 'ahuin' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getAhuiBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'ahuin' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// biyiinJin 
export const getBjin = async () => {
	return await client.fetch(
		`*[_type == 'biyiin' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getBjinBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'biyiin' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// cran 
export const getCran = async () => {
	return await client.fetch(
		`*[_type == 'kran' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getCranBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'kran' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// jingiin tootsooluur 
export const getJingiinTootsooluur = async () => {
	return await client.fetch(
		`*[_type == 'jingiinTootsooluur' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getJingiinTootsooluurByslug = async (slug) => {
	return await client.fetch(
		`*[_type == 'jingiinTootsooluur' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// jingiin dakchik 
export const getDatchik = async () => {
	return await client.fetch(
		`*[_type == 'jingiinDatchik' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getDatchikBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'jingiinDatchik' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// smart  
export const getSmart= async () => {
	return await client.fetch(
		`*[_type == 'UhaalagTooluur' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getSmartBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'UhaalagTooluur' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
// jingiin tuslah heregsel 
export const getTuslah = async () => {
	return await client.fetch(
		`*[_type == 'jingiinTuslahHeregsel' ]{name, bio, "slug": slug.current, 'image': image.asset->url ,'youtube':youtube.url}`	
		)
}
export const getTuslahBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'jingiinTuslahHeregsel' && slug.current==$slug ]{name,  "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
// end
export const getAllDelgvvr = async () => {
	return await client.fetch(
		`*[_type == 'delgvvr' ]{name, bio, "slug": slug.current, 'image': image.asset->url }`	
		)
}
export const getAllLogo = async () => {
	return await client.fetch(
		`*[_type == "logo"] {name, "image": image.asset->url}`
		)
}
export const getAllNews = async () => {
	return await client.fetch(
		`*[_type == 'news' ]{name, bio, "slug": slug.current, 'image': image.asset->url }`
		)
}
export const getAllShop = async () => {
	return await client.fetch(
		`*[_type == 'shop' ]{name, bio, "slug": slug.current, 'image': image.asset->url }`
		)
}
export const getAllGerchilgee = async () => {
	return await client.fetch(
		`*[_type == 'gerchilgee' ]{name, bio, "slug": slug.current, 'image': image.asset->url }`
		)
}

export const getPostBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'news' && slug.current==$slug ]{name, bio, "slug": slug.current, 'image': image.asset->url }`
		,{slug}
		);
		return postMessage;
}
export const getProductBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'product' && slug.current==$slug ]{name, bio, "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
export const getOlonBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'olonTaript' && slug.current==$slug ]{name, bio, "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}

export const getShopBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'shop' && slug.current==$slug ]{name, bio, "slug": slug.current, 'image': images[].asset->url }`
		,{slug}
		);
	
}
export const getGerchilgeeBySlug = async (slug) => {
	return await client.fetch(
		`*[_type == 'gerchilgee' && slug.current==$slug ]{name, bio, "slug": slug.current, 'image': image.asset->url }`
		,{slug}
		);
}
export const getUhaalagTooluurBySlug = async (slug) => {
	return await client.fetch(
		
		`*[_type == 'UhaalagTooluur'  ]{name, bio, 'image': image.asset->url }`
		
		);
	
}

// KEY "4512ad5f707e"
// Slug "гоо-сайханы-үйлчилгээ"