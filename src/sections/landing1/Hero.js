import React from "react";
import Head from "next/head";
import TestCarousel from "../../components/Slider/TestSlider";
import { useMeasure } from "@reactivers/use-measure";
import { useRef } from "react";

const Hero = () => {
  const ref = useRef(null);

  const measure = useMeasure({
    ref,
    updateOnWindowResize: true,
  });
  return (
    <>
    <Head>
    <link
            rel="preload"
            href="https://fonts.googleapis.com/css2?family=Lobster&family=Roboto:wght@100&display=swap"
            as="font"
            crossOrigin=""
          />
    </Head>
      <div className="w-full auto relative ">
        <div id="bg" className="grid grid-cols-2">
          <div className="h-full relative">
            <div
              style={{
                width: "65%",
                maxWidth: "439px",
              }}
              className="h-40 absolute left-20 right-0 top-0 bottom-0 m-auto z-20"
            >
              {/* <p
                style={{ lineHeight: "1.5", fontWeight: "500" }}
                className=" meijvr-head-text  lg:text-lg md:text-base sm:text-xs text-white"
              >
                Бид бүгдийг хэмжинэ
              </p> */}
             <img ref={ref} src="https://cdn.sanity.io/images/xm2z006s/production/6cf4457ae6b92010dd04f99ffccc9f27e59531e2-409x64.png" className="absolute uria left-0 top-0 z-10" />

            </div>
          </div>
          <TestCarousel height={`${Math.floor(JSON.stringify(measure.height))}px`} />
        </div>
        <img ref={ref} src="https://cdn.sanity.io/images/xm2z006s/production/b2ac162ff5ab41dc888f005ec6b175fd9e244c27-2560x910.png" className="absolute left-0 top-0 z-10" />
      </div>
      <style jsx>{`
@import url('https://fonts.googleapis.com/css2?family=Lobster&family=Roboto:wght@100&display=swap');
        #bg {
          grid-template-columns: 1fr 2.5fr;
        }
        .meijvr-head-text {


          
font-family: 'Lobster'!important;
          font-size:38px;
        }
      .Button__ButtonStyled-sc-l325es-0{
        color:#376bc4!important
      }
        .swiper {
          width: 100%;
          height: 100%;
        }

        .swiper-slide {
          background-position: center;
          background-size: contain;
        }

        .swiper-slide img {
          display: block;
          width: 200px;
        }

        .grid2 {
          max-width: 1000px;
        }

        #imgSlider {
          max-width: 100%;
          height: auto;
        }
        @media screen and (max-width: 767px) {
          .uria{
            display:none;
          }
         
        }
       
      `}</style>
    </>
  );
};

export default Hero;
