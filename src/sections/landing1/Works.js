import React, { useState, useEffect } from "react";
import { Button,Center, Flex, SimpleGrid, useColorModeValue } from "@chakra-ui/react";
import { designWorks1 } from "../../data";
import Slider from "../../components/Slider";
import NewsSlider from "../../components/Slider/news-slider";
import Partners from "../../components/Slider/PartnerSlider";
import Hurd from "../../components/Card/hudaldaa2";
import Tsahilgaan from "../../components/Card/tsahilgaan";
import Us from "../../components/Card/us";
import Auto from "../../components/Card/auto-mashin";
import { useMediaQuery } from "react-responsive";
import Link from "next/dist/client/link";
import BlockContent from "@sanity/block-content-to-react";
import getYouTubeId from "get-youtube-id";
import YouTube from "react-youtube";
const company = [
  "https://cdn.sanity.io/images/xm2z006s/production/986880f990ed116e66e3eece819eb9011292c20a-142x148.jpg",
  "https://cdn.sanity.io/images/xm2z006s/production/e82f16f3887a95c9931d189d8cc71e4132a617f6-142x148.jpg",
  "https://cdn.sanity.io/images/xm2z006s/production/c97573fe398170f297eff0c90d9b306fd56934b8-142x148.jpg",
  "https://cdn.sanity.io/images/xm2z006s/production/884feae3a4aeff3af5f4afa77dd3887a115de89c-142x148.jpg",
  "https://cdn.sanity.io/images/xm2z006s/production/7cde1b64926d12b0f7f225c73cd83094846b58f5-142x148.jpg",
  "https://cdn.sanity.io/images/xm2z006s/production/ebbbd99a846c6bc9537c91ef12b9f9aac0e116a3-142x148.jpg",
  "https://cdn.sanity.io/images/xm2z006s/production/1a567e21279e5908a32ad4652382e10f013faafd-142x148.jpg",
  "http://amjiltacademy.com/wp-content/uploads/2021/12/tom-amjilt-logo.png",
  "https://cdn.sanity.io/images/xm2z006s/production/a61ecbff24926c1cc0be7220265ccda8aa7510da-142x148.jpg",
  "https://cdn.sanity.io/images/xm2z006s/production/76ea451cb2e6e01dede4eb765eab5ba43ad3e76d-142x148.jpg",

"https://cdn.sanity.io/images/xm2z006s/production/c4782d39a5eef24941f9ac22bbfd970e30ee8126-142x148.jpg",
"https://cdn.sanity.io/images/xm2z006s/production/91c622725dc45ccdf8b126d46a8139651f727fe3-142x148.jpg",
"https://cdn.sanity.io/images/xm2z006s/production/c092a2cca8e1daf8aad30ae17b8bb03593caf83d-142x148.jpg",
"https://cdn.sanity.io/images/xm2z006s/production/4da2450de932538a93d392e78b73c1e84d4f40f9-142x148.jpg",
"https://cdn.sanity.io/images/xm2z006s/production/2689bd2d746586e41bcf21fe99dc5ac11f2129e9-142x148.jpg",
"https://cdn.sanity.io/images/xm2z006s/production/0f3d680ad0751c2da31b14947a26ae866472eeea-142x148.jpg",
"https://cdn.sanity.io/images/xm2z006s/production/82891e9ea730655eaa1e270a3923c5461da63a66-142x148.jpg",
"https://cdn.sanity.io/images/xm2z006s/production/76dc7cb872aaf93999daba11eda1cf23fa68d652-142x148.jpg",
"https://cdn.sanity.io/images/xm2z006s/production/68379424a0a9f222a52de7ce8956cd6723f90faa-142x148.jpg"
];
const Works = ({ main,data,medee,shop ,gerchilgee}) => {

  const Desktop = ({ children }) => {
    const isDesktop = useMediaQuery({ minWidth: 901 });
    return isDesktop ? children : null;
  };
  const Portrait = ({ children }) => {
    const Portrait = useMediaQuery({ maxWidth: 900 });
    return Portrait ? children : null;
  };

  const Mobile = ({ children }) => {
    const Portrait = useMediaQuery({ maxWidth: 767 });
    return Portrait ? children : null;
  };

  const [items, setItems] = useState([]);
  const [activeLink, setActiveLink] = useState("*");

  useEffect(() => {
    setItems(designWorks1);
  }, []);

  const filterBy = (cat) => {
    if (cat === "*") {
      setActiveLink("*");
      setItems(designWorks1);
    } else {
      const filteredItems = designWorks1.filter((item) => {
        return item.categories.indexOf(cat) !== -1;
      });
      setActiveLink(cat);
      setItems(filteredItems);
    }
  };

  const masonryOptions = {
    transitionDuration: 1000,
  };
  const serializers = {
		types: {
		
			youtube: ({ node }) => {
				const { url } = node;
				const id = getYouTubeId(url);
				return <YouTube videoId={id} opts={opts} className={styles.youtube} />;
			},
		},
	};
  return (
    <>
      <div style={{ backgroundColor: "#e6e6e6" }}>
        {/* <!-- Works Area --> */}
        <div
          style={{
            maxWidth: "1920px",
            justifyContent: "evenly",
          }}
        >
          <div data-aos="zoom-in ">
          <div className="container ">
            <h1 className="product-title">БҮТЭЭГДЭХҮҮН</h1>
            {/* <Flex> */}

            <Slider comps={data} length={5} />
            {/* </Flex> */}
          </div>
          </div>
        </div>
        <div className="sda flex flex-col items-center justify-center  relative">
            <img className="our-image" src="https://cdn.sanity.io/images/xm2z006s/production/e3b81748683993c36e1919c7c9b2eae81b71d3c1-3508x1538.png" />
            <div
              id="innerImgText"
              className="w-4/5 xl:h-64 lg:h-56 md:h-48 sm:h-36 h-24 flex flex-col"
            >
              <div className="bidnii-tuhai">
              <p className="baga-text our text-white text-2xl font-medium border-b-2 text-center mb-4 pb-1">
                БИДНИЙ ТУХАЙ
              </p>
              <p
                style={{ lineHeight: "1.8", textShadow: "1px 1px #000" }}
                className="text-white mr-10  ml-10 text-xl mb-5"
              >
                <span className=" our ">КОМПАНИЙН ТУХАЙ:</span>
                <span className=" our  ">
                 Манай компани нь 2008 онд
                байгуулагдсан бөгөөд 2011 оноос Хэмжлийн нэгдмэл байдлыг хангах тухай хууль, Аж
                ахуйн үйл ажиллагааны тусгай зөвшөөрлийн тухай хуулийн дагуу Засгийн газрын
                тохируулагч агентлаг Стандартчилал,  хэмжил зүйн газраас Монгол Улсад ашиглахыг
                зөвшөөрсөн загварын
                </span>
              </p>
              <div className="sda2">
              <Center>
              <Button
                className=""
                style={{
                  borderRadius: ".40rem",
                  transition: "300ms",
                }}
                w={"full"}
                mt={-10}
                mb={5}
                px="36"
                py="2"
                mx="4"
                bg={useColorModeValue("#376bc4", "gray.900")}
                color={"white"}
                _hover={{
                  transform: "translateY(-2px)",
                }}
              >
                <Link href="/our-about">
                  <a className="link-a">Дэлгэрэнгүй...</a>
                </Link>
                
              </Button>
            </Center>
            </div>
              </div>
              
            
          </div>
          
        </div>
        <style jsx>{`
       
         .link-a {
          color: white !important;
        }
        .ikElqi{
          background-color: blue!important;
        }
 
        .baga-text{
          font-weight: bold;
        }
        span{
          font-weight: bold;
        }
        .bidnii-tuhai{
          background:#115ACA;
          opacity:0.7;
          border-radius:20px;
        }
        .sda{
          /* style={{ margin: '2rem', backgroundImage: 'url()' }} */
          /* margin: 2rem; */
          background-image: url("https://cdn.sanity.io/images/xm2z006s/production/a4addb1c8fc39b27ccdaaca5e49833e08a935fe8-5778x3017.jpg");
          background-size: 100%;
          color: #fff;
          background-repeat: no-repeat;
          background-position: center;
          background-size: cover;
          background-attachment: fixed;
        }
       
          ::-webkit-scrollbar {
            width: 10px;
          }

          /* Track */
          ::-webkit-scrollbar-track {
            background: #d4d4d4;
            border-radius: 3px;
          }

          /* Handle */
          ::-webkit-scrollbar-thumb {
            background: #696969;
            border-radius: 3px;
          }

          /* Handle on hover */
          ::-webkit-scrollbar-thumb:hover {
            background: #555;
          }
          #innerImgText {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            overflow-y: auto;
          }
          #scrollBarHide {
            overflow-y: auto;
          }

          #scrollBarHide::-webkit-scrollbar {
            opacity: 0.8;
          }
          #innerImgBG {
            background-size: contain;
            background-repeat: no-repeat;
            width: 100%;
            height: 0;
            padding-top: 50%; /* (img-height / img-width * container-width) */
            position: relative;
          }
          .our-p {
            color: white;
          }
          .our-h2 {
            color: white;
            font-size: 20px;
          }
          .sda {
            position: relative;
          }
          .product-title {
            color: #4a77fa;
            font-size: 18px;
            margin-left: 40px;
          }
          .news-title {
            color: #4a77fa;
            font-size: 18px;
            margin-left: 40px;
            text-align: center;
          }
          .medee-container {
            display: flex;
            align-items: center;

            justify-content: center;
          }
         
          .our-background {
            position: absolute;
            background-color: #0d6af2;
            opacity: 0.7;
            text-align: center;
            height: 180px;
            border-radius: 50px;
            width: 80%;
          }
          @media screen and (max-width: 767px) {
           .our{
             font-size:10px;
            
           }
          }
        `}</style>
     <div data-aos="fade-up"
     data-aos-duration="3000">
        <div className="container ">
        <h1 className="news-title ">МЭДЭЭ МЭДЭЭЛЭЛ</h1>
            {/* <Flex> */}
          
            <NewsSlider comps={medee} length={3} />
            {/* </Flex> */}
      
        </div>
        </div>
        {/*end*/}<div data-aos="fade-down"
     data-aos-easing="linear"
     data-aos-duration="1500">

        <h1 className="news-title mt-5">ХУДАЛДАА ҮЙЛЧИЛГЭЭ</h1>
        
          <div className="row  flex items-center">
      
            <div className="col-sm-12 col-md-9">
            {/* <Slider comps={data} length={4} /> */}
            <div className="grid grid-cols-1 md:grid-cols-4 p-1 content-center">
            <Hurd/>
            <Tsahilgaan/>
            <Us/>
            <Auto/>
            {/* <div className="col-span-3 my-auto ">
            <HudaldaaSlider comps={shop} length={3} className="col-span-3" />
            </div> */}
            </div>
         
            </div>
            <Desktop>
            <div className="col-sm-12 col-md-3">
            <iframe width="300" height="300" 
            src={main[0]?.youtube} 
            title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            
            </div>
            </Desktop>
            <Mobile>
            <div className="col-sm-12 col-md-3">
              <iframe
                className="w-full"
                height="280"
                src={main[0]?.youtube}
                title="YouTube video player"
                frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
              ></iframe>
            </div>
            </Mobile>
          </div>
        </div>
        </div>
        <div className="mt-7  w-full flex flex-col items-center bg-white pt-8">
          <h1 className="text-blue-500 text-lg border-b-2 border-blue-500 text-center">
            ХАМТЫН АЖИЛЛАГАА
          </h1>
          <Partners comps={company} />
        </div>

       
     
    </>
  );
};
export default Works;
