import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { getAllProduct } from '../../../lib/api';
import { Container,Row,Col } from 'react-bootstrap';
import Slider from '../Slider/analytic';
import OndorSlider from '../Slider/ondor';
import HudaldaaJinSlider from '../Slider/hudaldaaJin';
import TawtsanS from '../Slider/tawtsan';
import LombardSlider from '../Slider/lombard';
import AhuiSlider from '../Slider/ahui';
import BjinSlider from '../Slider/BiyiinJin';
import CranSlider from '../Slider/cran';
import JInSlider from '../Slider/jingiinTootsooluur';
import DatchikSlider from '../Slider/datchik';
import TuslahSlider from '../Slider/tuslah';
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

export default function VerticalTabs({analytic,ondor,tuslah,hudaldaaJin,tawtsan,lombard,ahui,bjin,cran,jingiinTootsooluur,datchik}) {
  
  const [value, setValue] = React.useState(0);
  console.log("tab tuslah",tuslah)

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
      <>
<Container>
      
  <Row>
  

    <Box
      sx={{ flexGrow: 1, bgcolor: 'background.paper', display: 'flex', height: 750 }}
    >
     
      <Tabs
        orientation="vertical"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        sx={{ borderRight: 1, borderColor: 'divider' }}
      >

        <Tab className='inline-block py-3 my-1 px-20 text-sm font-medium text-center text-white  tab-color  rounded-lg ' label="Аналитик жин" {...a11yProps(0)} />
        <Tab  className='inline-block py-3 my-1 px-4 text-sm font-medium text-center text-white  tab-color  rounded-lg active' label="Өндөр нарийвчлалтай жин" {...a11yProps(1)} />
        <Tab className='inline-block py-3 my-1 px-4 text-sm font-medium text-center text-white  tab-color  rounded-lg ' label="Худалдааны жин" {...a11yProps(2)} />
        <Tab className='inline-block py-3 my-1 px-4 text-sm font-medium text-center text-white  tab-color  rounded-lg ' label="Тавцант жин" {...a11yProps(3)} />
        <Tab className='inline-block py-3 my-1 px-4 text-sm font-medium text-center text-white  tab-color  rounded-lg ' label="Ломбардны жин" {...a11yProps(4)} />
        <Tab className='inline-block py-3 my-1 px-20 text-sm font-medium text-center text-white  tab-color  rounded-lg ' label="Ахуйн жин" {...a11yProps(0)} />
        <Tab  className='inline-block py-3 my-1 px-4 text-sm font-medium text-center text-white  tab-color  rounded-lg active' label="Биеийн жин" {...a11yProps(1)} />
        <Tab className='inline-block py-3 my-1 px-4 text-sm font-medium text-center text-white  tab-color  rounded-lg ' label="Кран жин" {...a11yProps(2)} />
        <Tab className='inline-block py-3 my-1 px-4 text-sm font-medium text-center text-white  tab-color  rounded-lg ' label="Жингийн тооцоолуур " {...a11yProps(3)} />
        <Tab className='inline-block py-3 my-1 px-4 text-sm font-medium text-center text-white  tab-color  rounded-lg ' label="Жингийн датчик " {...a11yProps(4)} />
        <Tab className='inline-block py-3 my-1 px-4 text-sm font-medium text-center text-white  tab-color  rounded-lg ' label="Жингийн туслах хэрэгсэл " {...a11yProps(4)} />

       
      </Tabs>
      <Container>
      <TabPanel value={value} index={0}>
      <Slider comps={analytic} length={4} />
      </TabPanel>
      <TabPanel value={value} index={1}>
      <OndorSlider comps={ondor} length={4} />

      </TabPanel>
      <TabPanel value={value} index={2}>
      <HudaldaaJinSlider comps={hudaldaaJin} length={4} />

      </TabPanel>
      <TabPanel value={value} index={3}>
      <TawtsanS comps={tawtsan} length={4} />
      </TabPanel>
      <TabPanel value={value} index={4}>
      <LombardSlider comps={lombard} length={4} />
      </TabPanel>
      <TabPanel value={value} index={5}>
      <AhuiSlider comps={ahui} length={4} />
      </TabPanel>
      <TabPanel value={value} index={6}>
      <BjinSlider comps={bjin} length={4} />
      </TabPanel>
      <TabPanel value={value} index={7}>
      <CranSlider comps={cran} length={4} />
      </TabPanel>
      <TabPanel value={value} index={8}>
      <JInSlider comps={jingiinTootsooluur} length={4} />
      </TabPanel>
      <TabPanel value={value} index={9}>
      <DatchikSlider comps={datchik} length={4} />
      </TabPanel>
      <TabPanel value={value} index={10}>
      <TuslahSlider comps={tuslah} length={4} />
      </TabPanel>
      </Container>
    </Box>
    </Row>
    </Container>
    </>
    
 
  );
}

