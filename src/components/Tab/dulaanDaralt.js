import * as React from 'react';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { getAllProduct } from '../../../lib/api';
import { Container,Row,Col } from 'react-bootstrap';
import Slider from '../Slider/manometr';
import DaraltSlider from '../Slider/daraltSlider';
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

export default function VerticalTabs({manometr,daralt}) {
    console.log('tab-daralt',daralt);
  
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
      <>
<Container>
      
  <Row>
  

    <Box
      sx={{ flexGrow: 1, bgcolor: 'background.paper', display: 'flex', height: 424 }}
    >
     
      <Tabs
        orientation="vertical"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        sx={{ borderRight: 1, borderColor: 'divider' }}
      >

        <Tab className='inline-block py-3 my-1 px-20 text-sm font-medium text-center text-white  tab-color  rounded-lg ' label="Манометр" {...a11yProps(0)} />
        <Tab  className='inline-block py-3 my-1 px-4 text-sm font-medium text-center text-white  tab-color  rounded-lg active' label="Даралтын процесс" {...a11yProps(1)} />
        <Tab className='inline-block py-3 my-1 px-4 text-sm font-medium text-center text-white  tab-color  rounded-lg ' label="Бусад тоног төхөөрөмж" {...a11yProps(2)} />
       
       
      </Tabs>
      <Container>
      <TabPanel value={value} index={0}>
      <Slider comps={manometr} length={4} />
      </TabPanel>
      <TabPanel value={value} index={1}>
      <DaraltSlider comps={daralt} length={4} />
      </TabPanel>
      <TabPanel value={value} index={2}>
      <Slider comps={manometr} length={4} />
      </TabPanel>     
      </Container>
    </Box>
    </Row>
    </Container>
    </>
    
 
  );
}


