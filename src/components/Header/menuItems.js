export const menuItems = [
  {
    name: "/",
    label: "Нүүр",
  },
  {
    name: "",
    label: "Бидний тухай ",
    items: [
      { name: "our-about", label: "Компаний танилцуулга" },
      { name: "gerchil", label: "тусгай зөвшөөрөл,гэрчилгээ" },
      { name: "turshlaga", label: "Ажлын туршлага" },
      { name: "hamtiin-ajillagaa", label: "Хамтын ажиллагаа" },
    ],
  },
  {
    name: "product",
    label: "Бүтээгдэхүүн",
    items: [
      {
        name: "electricity_count",
        label: "Цахилгаан тоолуур",
        items: [
          {
            name: "smart",
            label: "Ухаалаг тоолуур",
          },
          {
            name: "simple",
            label: "энгийн электрон тоолуур",
          },
          {
            name: "simple",
            label: "олон тарифт тоолуур",
          },
          {
            name: "collector",
            label: "Коллектор",
          },
          {
            name: "concentrator",
            label: "Концентратор",
          },
        ],
      },
      {
        name: "electron",
        label: "Электрон жин",
        items: [
          { name: "electron", label: "Аналитик жин" },
          { name: "electron", label: "Өндөр нарийвчлалтай жин" },
          { name: "electron", label: "Худалдааны жин" },
          { name: "electron", label: "Тавцант жин" },
          { name: "electron", label: "Ломбардны жин" },
          { name: "electron", label: "Ахуйн жин" },
          { name: "electron", label: "Биеийн жин" },
          { name: "electron", label: "Кран жин" },
          { name: "electron", label: "Авто машины жин" },
          { name: "electron", label: "Жингийн тооцоолуур" },
          { name: "electron", label: "Жингийн датчик" },
          { name: "electron", label: "Жингийн туслах хэрэгсэл" },
        ],
      },
      {
        name: "water_count",
        label: "Усны тоолуур",
        items: [
          {
            name: "water-smart",
            label: "Ухаалаг",
          },
          {
            name: "water-simple",
            label: "энгийн",
          },
        ],
      },
      { name: "dulaan-daralt", label: "Дулаан даралт",
      items: [
        { name: "manometr", label: "Манометр" },
        { name: "manometr", label: "Даралтын процесс" },
        { name: "manometr", label: "Бусад тоног төхөөрөмж " },
    
      ],
    },
      { name: "software", label: "Програм хангамж",
      items: [
        { name: "auto-mashin-jin", label: "Авто машины жингийн програм хангамж" },
        { name: "auto-mashin-jin", label: "ТАМАДУМ систем" },
        { name: "auto-mashin-jin", label: "Авто машины ухаалаг гарцын систем" },
      
      ],
    },
      
      {
        name: "zasvar-vilchilgee",
        label: "Засвар үйлчилгээ",
        items: [
          {
            name: "tsahilgaa-tooluuriin-lab",
            label: "Цахилгаан тоолуурын лаборатори",
          },
          {
            name: "usnii-tooluur-lab",
            label: "Усны тоолуурын лаборатори",
          },
          {
            name: "jingiin-zasvar",
            label: "Жингийн засвар",
          },
        ],
      },
      {
        name: "busad-zaswar",
        label: "Бусад  үйлчилгээ",
        
      },
    ],
  },
  {
    name: "medee",
    label: "Мэдээ мэдээлэл",
  },
  {
    name: "contact",
    label: "Холбоо барих",
  },
];
