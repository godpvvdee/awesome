import React from "react";
import Slider from "react-slick";
import {
    Box,
    IconButton,
    useBreakpointValue,
    Stack,
    Heading,
    Text,
    Container,
  } from '@chakra-ui/react';
export default function SecondSlider() {
   
  var settings = {
    dots: true,
    infinite: true,
    // autoplay: true,
    // autoplaySpeed: 1000,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false
  };
  const cards = [
    {
      title: 'Дэлгүүр 2',
   
        image:
      "https://cdn.sanity.io/images/xm2z006s/production/f88a02553aabdb5712a6c59464db71bd3413c41c-3937x1981.jpg"   },
    {
  
     
        image:
      "https://cdn.sanity.io/images/xm2z006s/production/1017617745a9a1dd0f8af352125d825cae6399c8-2362x1577.jpg"   },
   
      {
 
    
        image:
      "https://cdn.sanity.io/images/xm2z006s/production/abef54db12f27a9465204bf6e082456cb58616a8-2362x1577.jpg"   },
      {
       
       
          image:
        "https://cdn.sanity.io/images/xm2z006s/production/131bab8b8c0b8afaff6282f50ab9182f35a5b8cb-2362x1577.jpg"},
     
    ];
  return (
     
    <Slider {...settings}>
         {cards.map((card, index) => (
          <Box
            key={index}
            height={'6xl'}
            position="relative"
            backgroundPosition="center"
            backgroundRepeat="no-repeat"
            backgroundSize="cover"
            backgroundImage={`url(${card.image})`}>
            {/* This is the block you need to change, to customize the caption */}
            <Container size="container.lg" height="600px" position="relative">
              <Stack
                spacing={6}
                w={'full'}
                maxW={'lg'}
                position="absolute"
                top="50%"
                transform="translate(0, -50%)">
                
             
              </Stack>
            </Container>
          </Box>
        ))}
    
    </Slider>
  );
}