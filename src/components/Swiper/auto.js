import React from "react";
import Slider from "react-slick";
import {
    Box,
    IconButton,
    useBreakpointValue,
    Stack,
    Heading,
 
    Text,
    Container,
  } from '@chakra-ui/react';
export default function SimpleSlider() {
   
  var settings = {
  
    dots: true,
    arrows: true,
    infinite: true,
    autoplay: true,
    speed: 500,
    autoplaySpeed: 4000,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false
  };
  const cards = [
    {
  
    
      
        image:
        "https://cdn.sanity.io/images/xm2z006s/production/098ec358fed560b846ffd9f502528fc5d3a62912-6016x4016.jpg"   },
    {
    
    
      image:
      "https://cdn.sanity.io/images/xm2z006s/production/9d2b960f00192e8ab032cdedf048381f604046ae-6016x4016.jpg" },
   
      {
    
      image:
      "https://cdn.sanity.io/images/xm2z006s/production/73989b1d1d22ad95f8a3c6e984887f450a5f8ba3-4128x3096.jpg"    },
      {
      
       
        image:
        "https://cdn.sanity.io/images/xm2z006s/production/9d2b960f00192e8ab032cdedf048381f604046ae-6016x4016.jpg" },
     
    ];
  return (
    
    <Slider {...settings}>
     

     
         {cards.map((card, index) => (
             <Stack spacing={8}>

          <Box className="slider-tsah"
            key={index}
            height={'6xl'}
            position="relative"        
            backgroundPosition="center"
            backgroundRepeat="no-repeat"
         
            backgroundSize="cover"
            
            backgroundImage={`url(${card.image})`}>
            {/* This is the block you need to change, to customize the caption */}
            <Container size="container.lg" height="400px" position="relative">
              <Stack
                spacing={6}
                w={'full'}
                maxW={'lg'}
                position="absolute"
                top="50%"
                transform="translate(0, -50%)">
                
              </Stack>
              
            </Container>
            
          </Box>     
          </Stack>     
        ))}

    </Slider>
    
  );
}
