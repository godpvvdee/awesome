import React from "react";
import Slider from "react-slick";
import {
    Box,
    IconButton,
    useBreakpointValue,
    Stack,
    Heading,
    Text,
    Container,
  } from '@chakra-ui/react';
export default function SimpleSlider() {
   
  var settings = {
    dots: true,
    infinite: true,
    // autoplay: true,
    // autoplaySpeed: 1000,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false
  };
  const cards = [
    {
      title: 'Дэлгүүр 1',
    
      
        image:
      "https://cdn.sanity.io/images/xm2z006s/production/37adeb9f0ecca981287ab1d6a72845cbd5e5dfd0-3543x1338.jpg"    },
    {
    
    
      image:
      "https://cdn.sanity.io/images/xm2z006s/production/6c9e579704479288919b01edfd86e019dbc718b4-1772x1183.jpg"    },
   
      {
    
      image:
      "https://cdn.sanity.io/images/xm2z006s/production/3f7f6451ffae7e85799494da815860d53f91aaa4-1772x1183.jpg"    },
      {
      
       
        image:
        "https://cdn.sanity.io/images/xm2z006s/production/2d444e1caeecc3a525546fbb1bcc253ba34fa060-1772x1183.jpg"},
     
    ];
  return (
     
    <Slider {...settings}>
         {cards.map((card, index) => (
          <Box
            key={index}
            height={'6xl'}
            position="relative"
            backgroundPosition="center"
            backgroundRepeat="no-repeat"
            backgroundSize="cover"
            backgroundImage={`url(${card.image})`}>
            {/* This is the block you need to change, to customize the caption */}
            <Container size="container.lg" height="600px" position="relative">
              <Stack
                spacing={6}
                w={'full'}
                maxW={'lg'}
                position="absolute"
                top="50%"
                transform="translate(0, -50%)">
                
                <Text fontSize='6xl'  color="#376bc4">
                  {card.title}
                </Text>
              </Stack>
              
            </Container>
            
          </Box>          
        ))}
   
    </Slider>
  );
}