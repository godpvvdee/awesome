import React from "react";
import Slider from "react-slick";
import {
    Box,
    IconButton,
    useBreakpointValue,
    Stack,
    Heading,
 
    Text,
    Container,
  } from '@chakra-ui/react';
export default function SimpleSlider() {
   
  var settings = {
  
    dots: true,
    infinite: true,
    // autoplay: true,
    // autoplaySpeed: 1000,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false
  };
  const cards = [ { image: "https://cdn.sanity.io/images/xm2z006s/production/fcd4737fa17d0633177b0b7f4c22ca2dc9aec134-1969x2949.jpg"}, {
    
    
      image:
      "https://cdn.sanity.io/images/xm2z006s/production/0c3d017a35053b44f293fc4a8b1b7985799e7bc8-2362x1577.jpg"   },
   
      {
    
      image:
      "https://cdn.sanity.io/images/xm2z006s/production/9792e978f50a244705316acfc1c81166e9c2fa85-2362x1577.jpg"    },
      {
      
       
        image:
        "https://cdn.sanity.io/images/xm2z006s/production/0c3d017a35053b44f293fc4a8b1b7985799e7bc8-2362x1577.jpg"   },
     
    ];
  return (
    
    <Slider {...settings}>
     

     
         {cards.map((card, index) => (
             <Stack spacing={8}>

          <Box className="slider-tsah"
            key={index}
            height={'6xl'}
            position="relative"        
            backgroundPosition="center"
            backgroundRepeat="no-repeat"
         
            backgroundSize="cover"
            
            backgroundImage={`url(${card.image})`}>
            {/* This is the block you need to change, to customize the caption */}
            <Container size="container.lg" height="400px" position="relative">
              <Stack
                spacing={6}
                w={'full'}
                maxW={'lg'}
                position="absolute"
                top="50%"
                transform="translate(0, -50%)">
                
              </Stack>
              
            </Container>
            
          </Box>     
          </Stack>     
        ))}

    </Slider>
    
  );
}
