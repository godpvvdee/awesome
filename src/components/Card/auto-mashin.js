import React from "react";
import Link from "next/dist/client/link";
import {
  Box,
  Center,
  Image,
  Badge,
  Button,
  useColorModeValue,
  Fade,
  ScaleFade,
  Slide,
  SlideFade,
  useDisclosure,
  Text,
  Stack,
  List,
  ListItem,
  ListIcon,
} from "@chakra-ui/react";
import { StarIcon } from "@chakra-ui/icons";
import { getAllShop } from "../../../lib/api";

const Auto = ({  }) => {
  const { isOpen, onToggle } = useDisclosure();
  const property = {
    imageUrl: "https://cdn.sanity.io/images/xm2z006s/production/7794a8c4e6a099248dcfb7761f8dc80b9415e4c0-1614x848.jpg",
    imageAlt: "hurd salbar",
    beds: 3,
    name:"авто машины жин засвар суурилалт",
    baths: 2,
    title: "Багаж",
    formattedPrice: "1,9000.00₮",
    reviewCount: 34,
    rating: 4,
  };
  return (
    <div>
      <Center py={6} boxShadow={"2xl"}>
        <Box
          maxW="sm"
          borderWidth="1px"
          borderRadius="lg"
          overflow="hidden"
          className="bg-white w-80 rounded-xl p-1 shadow-xl"
        >
          <img className="w-80 h-52" src={property.imageUrl} alt={property.imageAlt} objectFit="cover" />

          <Box p="3">
            <Box
              mt="1"
              ml="3"
              fontWeight="semibold"
              as="h6"
              height="40"
              textAlign={"left"}
              lineHeight="tight"
              
            >
              {property.name}
            </Box>

            <Center>
              <Button
                className=""
                style={{
                  borderRadius: ".40rem",
                  transition: "300ms",
                }}
                w={"full"}
                mt={20}
                mb={5}
                px="36"
                py="2"
                mx="4"
                bg={useColorModeValue("#376bc4", "gray.900")}
                color={"white"}
                _hover={{
                  transform: "translateY(-2px)",
                }}
              >
                     <Link href="/auto-mashin">
                  <a className="link-a">Дэлгэрэнгүй...</a>
                </Link>
                
                <style jsx>{`
                  .link-a {
                    color: white !important;
                  }
                `}</style>
              </Button>
            </Center>
          </Box>
        </Box>
      </Center>
    </div>
  );
};
export default Auto;