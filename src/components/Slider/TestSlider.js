import AwesomeSlider from "react-awesome-slider";
import "react-awesome-slider/dist/styles.css";
import withAutoplay from "react-awesome-slider/dist/autoplay";

const AutoplaySlider = withAutoplay(AwesomeSlider);

const slider = (props) => {
  const styles = (
    <style jsx>{`
      .aws-btn {
        --slider-height-percentage: 100%;
        --slider-transition-duration: 100ms;
        --organic-arrow-thickness: 4px;
        --organic-arrow-border-radius: 0px;
        --organic-arrow-height: 40px;
        --organic-arrow-color: #26456f;
        --control-button-width: 10%;
        --control-button-height: 25%;
        --control-button-background: transparent;
        --control-bullet-color: #2d5182;
        --control-bullet-active-color: #26456f;
        --loader-bar-color: #851515;
        --loader-bar-height: 6px;
      }
    `}</style>
  );
  return (
    <>
      <AutoplaySlider
        style={{ height: `${props.height}` }}
        play={true}
        cancelOnInteraction={false} // should stop playing on user interaction
        transitionDelay={2000}
        interval={1000}
        cssModules={styles}
        onTransitionEnd={() => console.log("hello")}
        buttons={false}
        bullets={false}
      >
        <div data-src="https://cdn.sanity.io/images/xm2z006s/production/110740224d1ac8790189921839037be226ae6c37-9338x5388.jpg"></div>
        <div data-src="https://cdn.sanity.io/images/xm2z006s/production/f5fe9b0c9b472aaf4268f659cbbbd18b7f47aa7b-1614x848.jpg"></div>
        <div data-src="https://cdn.sanity.io/images/xm2z006s/production/90f81eba56a8723d0c1c62bf2be4a578ccb24eeb-2048x1367.jpg"></div>
      </AutoplaySlider>
    </>
  );
};

export default slider;
