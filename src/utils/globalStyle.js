import { createGlobalStyle } from "styled-components";
import font from "../assets/fonts/icon-font/fonts/Grayic.eot"
const globalStyle = createGlobalStyle`

.icon-chat-round-2{
  display:none;
}
.ThemeSwitch__ButtonToggle-sc-127c3oe-5{
  display:none;
}


font-family: 'Mogul';

  src: url('../assets/fonts/icon-font/fonts/Mogul-Andantino-script.ttf');
  font-weight: normal;
  font-style: normal;
}

  body {
    background-color: ${({ theme }) => theme.colors.bg} !important;

    color: ${({ theme }) => theme.colors.text} !important;
  }
    .ikElqi{
      background-color:#376bc4!important;
    }
    .kHkZIj{
      background-color:#376bc4!important;

    }
  p, .p{
    color: ${({ theme }) => theme.colors.text};
    font-size: 18px;
    font-weight: 300;
    letter-spacing: -0.56px;
    line-height: 30px;
    margin-bottom: 0;
  }

  ul,.ul{
    list-style: none;
    margin: 0;
    padding: 0;
  }
 .NestedMenu___StyledListGroupItem3-sc-tka6c3-3{
   display:none;
 }
.NestedMenu___StyledListGroupItem4-sc-tka6c3-4{
  display:none;
}
  a {
    transition: all 0.3s ease-out;
    color: ${({ theme }) => theme.colors.heading} !important;
    &:hover, &:active, &:focus{
      text-decoration: none!important;
      outline: none !important;
      color: ${({ theme }) => theme.colors.primary} !important;
    }
  }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      /* display: none; <- Crashes Chrome on hover */
      -webkit-appearance: none;
      margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
    }

    input[type="number"] {
      -moz-appearance: textfield; /* Firefox */
    }
   
    .tab-color{
      background: rgb(45, 133, 192);
      background: linear-gradient(
        45deg,
        rgba(45, 133, 192, 1) 0%,
        rgba(18, 87, 151, 1) 24%,
        rgba(35, 115, 176, 1) 24%,
        rgba(17, 119, 175, 1) 61%,
        rgba(33, 139, 193, 1) 61%,
        rgba(88, 204, 255, 1) 100%
      );
    }
`;

export default globalStyle;
