import React from "react";
import { Element } from "react-scroll";
import Head from "next/head"
import PageWrapper from "../components/PageWrapper";
import Hero from "../sections/landing1/Hero";
import Works from "../sections/landing1/Works";
import Contact from "../sections/landing1/Contact";
import { getAllProduct,getAllNews, getAllShop, getAllGerchilgee, getJinTO, getMain  } from "../../lib/api";
const IndexPage = ({data,medee,shop,gerchilgee,main}) => {
  return (
    <>
    <Head>
        <title>Measurement.mn</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <PageWrapper>
        <Hero />
        <Element name="works">
          <Works data={data} medee={medee} shop={shop} gerchilgee={gerchilgee} main={main}/>
        </Element>

        <Contact />
      </PageWrapper>
    </>
  );
};
export default IndexPage;
export const getServerSideProps = async({ params}) =>{
// export async function getStaticProps(context) {
  const data = await getAllProduct(params);
  const medee = await getAllNews(params);
  const shop = await getAllShop(params);
  const gerchilgee = await getAllGerchilgee(params);
  const main = await getMain();
  return {
    props: {
      data,
      shop,
      medee,
      gerchilgee,
      main,
     
    }, 
  }
}
// }

