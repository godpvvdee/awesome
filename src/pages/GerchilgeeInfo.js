import { useMediaQuery } from "react-responsive";
import { useState } from "react";
import BigImg from "../assets/image/png/Gerch.jpg";

import PageWrapper from "../components/PageWrapper";
import Slider3 from "../components/Slider/news-slider";
import Link from "next/dist/client/link";
import News from "../components/Card/news";

import { Portal } from "@chakra-ui/portal";

const medee = [<News />, <News />, <News />, <News />, <News />, <News />];

const Example = () => {
  const Desktop = ({ children }) => {
    const isDesktop = useMediaQuery({ minWidth: 767 });
    return isDesktop ? children : null;
  };
  const Tablet = ({ children }) => {
    const isTablet = useMediaQuery({ minWidth: 768, maxWidth: 991 });
    return isTablet ? children : null;
  };
  const Mobile = ({ children }) => {
    const isMobile = useMediaQuery({ maxWidth: 767 });
    return isMobile ? children : null;
  };
  const Default = ({ children }) => {
    const isNotMobile = useMediaQuery({ minWidth: 768 });
    return isNotMobile ? children : null;
  };
  const Portrait = ({ children }) => {
    const isPortrait = useMediaQuery({ query: "(orientation: portrait)" });
    return isPortrait ? children : null;
  };

  const detail = [
    {
    },
    
  ];

  const images = [
    "https://cdn.sanity.io/images/xm2z006s/production/84479990ed03f4fd5ab1b1a98360622b31402c30-679x960.jpg"
   
];

  const [currentImage, setCurrentImage] = useState(images[0]);
  return (
    <div>
      <Desktop>
        {" "}
        <>
        <div className="w-full">
        <img id="smartImgShadow" src={BigImg} />
      </div>
          <div id="topMargin" className="mx-auto h-96 mb-40">
            <h1 className="text-sm text-blue-500 text-normal ml-2 mb-4">
            <h1 className="text-2xl text-blue-600 mb-3">Хэмжих хэрэгслийн загварын баталгааны гэрчилгээ – II</h1>

            </h1>
            <div id="mainDiv" className=" ">
              <div className="">
               
                <img className="w-full  mb-2" id="mainImg" src={currentImage} alt="abc" />
           
                
              </div>
              <div className="flex flex-col justify-between">
                <div className="w-full mx-auto">
                  {detail.map((data) => {
                    return (
                      <p className="font-bold text-black text-base">
                        {data.title}:<span className="font-light"></span>
                      </p>
                    );
                  })}
                  
                </div>              
              </div>
            </div>
          </div>
         
          {/* CSS */}
          <style jsx>{`
         
          .link-a {
            color: white !important;
          }
    
            .zurag {
              max-width: 60%;
              -moz-transition: all 0.9s;
              -webkit-transition: all 0.9s;
              transition: all 0.9s;
            }
            #mainImg {
              border-radius: 30px;
              transition: transform 2s, filter 1.5s ease-in-out;
              transform-origin: center center;
             
            }
           
            .product-title {
              color: #4a77fa;
              font-size: 18px;
              margin-left: 40px;
              text-align: center;
            }
            #topMargin {
              margin-top: 115px;
            
              height: fit-content;
              width: 40%;
             
            }
            #mainDiv {
              grid-template-columns: 1fr 1fr;
            
            }
            #mainImg {
              border-radius:20px;
              height: 800px;
           
            }
            #smallImg {
              object-fit: cover;
              border-radius:20px;
            }
            #slider {
              margin-bottom: 5rem;
            }
          `}</style>
        </>
      </Desktop>
      <Portrait>
        <>
          <div className="flex flex-col w-full mt-20 border pt-2">
            <h1 className="text-sm text-blue-500 text-normal ml-2 mb-3">
            <h1 className="text-2xl text-blue-600 mb-3">Паркын зүүн талд барих гүүрэн гарцны тухай хэлэлцэж байна</h1>

            </h1>
            </div>
            <img className=" mb-1 border-4 border-gray-500" src={currentImage} alt="abc" />
            
          
          <div className="mx-auto my-4 w-11/12">
         
            {detail.map((data) => {
              return (
                <p className="font-bold text-black text-base">
                  {data.title}:<span className="font-light"></span>
                </p>
              );
            })}
            
          </div>
          {/* end */}
          
        </>
      </Portrait>
    </div>
  );
};

export default Example;
