import { useMediaQuery } from "react-responsive";
import { useState } from "react";
import PageWrapper from "../components/PageWrapper";
import Slider3 from "../components/Slider/news-slider";
import BlockContent from "@sanity/block-content-to-react";

import Link from "next/dist/client/link";
import News from "../components/Card/news";
import { Button, useColorModeValue } from "@chakra-ui/react";
import { Portal } from "@chakra-ui/portal";
import { getAllNews } from "../../lib/api";
import Head from "next/head";
// const medee = [<News />, <News />, <News />, <News />, <News />, <News />];

const Example = ({ medee }) => {
  console.log("news medee bio,", medee.bio);
  
  const Desktop = ({ children }) => {
    const isDesktop = useMediaQuery({ minWidth: 767 });
    return isDesktop ? children : null;
  };
  const Tablet = ({ children }) => {
    const isTablet = useMediaQuery({ minWidth: 768, maxWidth: 991 });
    return isTablet ? children : null;
  };
  const Mobile = ({ children }) => {
    const isMobile = useMediaQuery({ maxWidth: 767 });
    return isMobile ? children : null;
  };
  const Default = ({ children }) => {
    const isNotMobile = useMediaQuery({ minWidth: 768 });
    return isNotMobile ? children : null;
  };
  const Portrait = ({ children }) => {
    const isPortrait = useMediaQuery({ query: "(orientation: portrait)" });
    return isPortrait ? children : null;
  };

  const detail = [
    {
      title: "Энэхүү бүтээн байгуулалтын ажилтай холбогдуулаад дөрөвдүгээр сарын 1-ний өдрөөс Үндэсний соёл амралтын хүрээлэнгийн зүүн тал буюу Олимпийн гудамжны авто замын хөдөлгөөнийг нэг жил зургаан сарын ху...",
    },

  ];

  const images = [
    "https://images.unsplash.com/photo-1546455643-312811da2332?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=636&q=80",
    "https://images.unsplash.com/photo-1521985068534-cec90327c93b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80",
    "https://images.unsplash.com/photo-1513323813850-c7318e3efc71?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80",
    "https://images.unsplash.com/photo-1612171709946-7fc1298a5a53?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=626&q=80",
  ];

  const [currentImage, setCurrentImage] = useState(images[0]);
  const serializers = {
		types: {
			code: (props) => (
				<pre data-language={props.node.language}>
					<code>{props.node.code}</code>
				</pre>
			),
		},
	};
  return (
    
    <div>
      <Head>
        <title>Measurement.mn</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Desktop>
        {" "}
        <>
          <div className="w-full">
            <img id="smartImgShadow" src="https://cdn.sanity.io/images/xm2z006s/production/0f33e3677209b7716990d1196483b32678d79a03-6140x2601.jpg" />
          </div>
          <div id="topMargin" className="mx-auto h-96 mb-40">
            <h1 className="text-sm text-blue-500 text-normal ml-2 mb-4">
              МЭДЭЭ МЭДЭЭЛЭЛ 
            </h1>
            {
              medee?.map((med, index) => (
                <div id="mainDiv" className=" gap-8 grid grid-cols-2" style={{marginBottom: '2rem'}}>
                  <div className="">
                    <div className="overflow-hidden">
                      <img className="w-full  mb-2" id="mainImg" src={med?.image} alt="abc" />
                    </div>

                  </div>

                  <div className="flex flex-col justify-between" key={index}>
                    <div className="w-full mx-auto">
                      <h1 className="text-2xl text-blue-600 mb-3">{med?.name}</h1>
                      {detail.map((data) => {
                        return (
                          <p className="font-bold text-black text-base">
<div style={{ maxWidth: "920px", margin: 'auto', textAlign: 'justify', lineHeight: '1.8rem' }}>
									<BlockContent blocks={med?.bio} serializers={serializers} />
								</div>    
                                        <span className="font-light"></span>
                          </p>
                        );
                      })}
                      <Button
                        className=""
                        style={{
                          borderRadius: ".40rem",
                          transition: "300ms",
                        }}
                        w={"full"}
                        mt={20}
                        mb={5}
                        px="36"
                        py="2"
                        mx="4"
                        bg={useColorModeValue("#376bc4", "gray.900")}
                        color={"white"}
                        _hover={{
                          transform: "translateY(-2px)",
                        }}
                      >
                        <Link href={`/news/${med.slug}`}>
                          <a className="link-a">Дэлгэрэнгүй...</a>
                        </Link>

                      </Button>
                    </div>
                  </div>
                </div>
              ))
            }

          </div>

          {/* CSS */}
          <style jsx>{`
         
          .link-a {
            color: white !important;
          }
    
            .zurag {
              max-width: 100%;
              -moz-transition: all 0.9s;
              -webkit-transition: all 0.9s;
              transition: all 0.9s;
            }
            #mainImg {
              border-radius: 30px;
              transition: transform 2s, filter 1.5s ease-in-out;
              transform-origin: center center;
              filter: brightness(50%);
            }
             #mainImg:hover {
              filter: brightness(100%);
              transform: scale(1.2);
              border-radius: 30px;
            }
            .product-title {
              color: #4a77fa;
              font-size: 18px;
              margin-left: 40px;
              text-align: center;
            }
            #topMargin {
              margin-top: 115px;
            
              height: fit-content;
              width: 80%;
             
            }
            #mainDiv {
              grid-template-columns: 1fr 1fr;
            
            }
            #mainImg {
              border-radius:20px;
              height: 270px;
              object-fit: cover;
              -webkit-transition: all 0.3s ease-out;
    -moz-transition: all 0.3s ease-out;
    -ms-transition: all 0.3s ease-out;
    -o-transition: all 0.3s ease-out;
    transition: all 0.3s ease-out;
            }
            #smallImg {
              object-fit: cover;
              border-radius:20px;
            }
            #slider {
              margin-bottom: 5rem;
            }
          `}</style>
        </>
      </Desktop>
      <Portrait>
        <>
          <div className="flex flex-col w-full mt-20 border pt-2">
            <h1 className="text-sm text-blue-500 text-normal ml-2 mb-3">
              МЭДЭЭ МЭДЭЭЛЭЛ
            </h1>
          </div>
          <img className=" mb-1 border-4 border-gray-500" src={currentImage} alt="abc" />


          <div className="mx-auto my-4 w-11/12">
            <h1 className="text-2xl text-blue-600 mb-3">Паркын зүүн талд барих гүүрэн гарцны тухай хэлэлцэж байна</h1>

            {detail.map((data) => {
              return (
                <p className="font-bold text-black text-base">
                  {data.bio}:<span className="font-light"></span>
                </p>
              );
            })}
            <Button
              className=""
              style={{
                borderRadius: ".40rem",
                transition: "300ms",
              }}
              w={"full"}
              mt={20}
              mb={5}
              px="36"
              py="2"
              mx="4"
              bg={useColorModeValue("#376bc4", "gray.900")}
              color={"white"}
              _hover={{
                transform: "translateY(-2px)",
              }}
            >
              <Link href={`/news/${medee.slug}`}>
                <a className="link-a">Дэлгэрэнгүй...</a>
              </Link>

            </Button>
          </div>


        </>
      </Portrait>
    </div>
  );
};

export default Example;

export async function getStaticProps(context) {
  const medee = await getAllNews();
  console.log('medee bio',medee);
  return {
    props: {
      medee

    },
  }

}

