import React, { Component } from 'react';
import Head from "next/head";

import {  getAhui, getAnalytic,getBjin,getCran,getDatchik,getHudaldaaJin, getJingiinTootsooluur, getLombard, getOndor, getTawtsan, getTuslah } from "../../lib/api";
import TabElec from '../components/Tab/electronJin';

const Electron = ({analytic,ondor,hudaldaaJin,tawtsan,tuslah,lombard,ahui,bjin,cran,jingiinTootsooluur,datchik}) => {
 var analytic2 = analytic;
var ondor2 = ondor;
var hudaldaaJin2 = hudaldaaJin;
var tawtsan2 = tawtsan;
var lombard2 = lombard;
var ahui2 = ahui;
var bjin2 = bjin;
var jingiinTootsooluur2 = jingiinTootsooluur;

var cran2 = cran;
console.log('main-jin-tuslah',tuslah);

  return (
    <>
    <Head>
        <title>Measurement.mn</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div className="w-full">
        <img id="smartImgShadow" src= "https://cdn.sanity.io/images/xm2z006s/production/0f33e3677209b7716990d1196483b32678d79a03-6140x2601.jpg"/>
      </div>
      <TabElec analytic={analytic2} ondor={ondor2} hudaldaaJin={hudaldaaJin2} tawtsan={tawtsan2} lombard={lombard2} ahui={ahui2} bjin={bjin2} cran={cran2} jingiinTootsooluur={jingiinTootsooluur2} datchik={datchik} tuslah={tuslah} ></TabElec>
      <style jsx>
        {`
          #smartImgShadow {
            -webkit-box-shadow: 0 6px 7px -6px black;
            -moz-box-shadow: 0 6px 7px -6px black;
            box-shadow: 0 6px 7px -6px black;
          }
        `}
      </style>
    </>
  );
};

export default Electron;


export const getServerSideProps = async({params})=> {
  const jingiinTootsooluur =  await getJingiinTootsooluur();

  const analytic = await getAnalytic();
const ondor =  await getOndor();
const hudaldaaJin  =await getHudaldaaJin();
const tawtsan =  await getTawtsan();
const lombard = await getLombard();
const ahui = await getAhui();
const bjin = await getBjin();
const cran = await getCran();
const datchik = await getDatchik();
const tuslah =  await getTuslah();
  return {
    props: {
      analytic,
      ondor,
      hudaldaaJin,
      tawtsan,
      lombard,
      ahui,
      bjin,
      cran,
      jingiinTootsooluur,
      datchik,
      tuslah,
    }, 
  }

}