import React, { Component } from 'react';
import Head from "next/head";
import Sidebar from "../components/Sidebar/Sidebar";
import Card from "../components/Card/uhaalagTooluur"
import Slider from "../components/Slider";
import { Container,Row,Col} from 'react-bootstrap';
import { Tabs, TabList, TabPanels, Tab, TabPanel } from '@chakra-ui/react'
import { getAllProduct, getautoMashiniiJin, getCollector, getConcentrator, getEngiinElectron, getOlonTaript, getTamadum, getUhaalagGarts } from "../../lib/api";
import VerticalTabs from '../components/Tab/programHangamj';

const autoMashiniiJin = (autoMashiniiJin,tamadum,uhaalagGarts) => {
  console.log('auto tamadum page',tamadum);
 var autoMashiniiJin2 = autoMashiniiJin.autoMashiniiJin;
var tamadum2 =  autoMashiniiJin.tamadum;
var uhaalagGarts2 = autoMashiniiJin.uhaalagGarts;
  return (
    <>
    <Head>
        <title>Measurement.mn</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div className="w-full">
        <img id="smartImgShadow" src= "https://cdn.sanity.io/images/xm2z006s/production/0f33e3677209b7716990d1196483b32678d79a03-6140x2601.jpg"/>
      </div>
      <VerticalTabs autoMashiniiJin={autoMashiniiJin2} tamadum={tamadum2} uhaalagGarts={uhaalagGarts2}></VerticalTabs>
      <style jsx>
        {`
          #smartImgShadow {
            -webkit-box-shadow: 0 6px 7px -6px black;
            -moz-box-shadow: 0 6px 7px -6px black;
            box-shadow: 0 6px 7px -6px black;
          }
        `}
      </style>
    </>
  );
};

export default autoMashiniiJin;


export const getServerSideProps = async({params})=> {
  const autoMashiniiJin = await getautoMashiniiJin();
 const tamadum = await getTamadum();
 const uhaalagGarts = await getUhaalagGarts();
  return {
    props: {
      autoMashiniiJin,
      tamadum,
      uhaalagGarts,
     
    }, 
  }

}