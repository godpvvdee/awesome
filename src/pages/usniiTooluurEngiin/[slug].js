import { useMediaQuery } from "react-responsive";
import { useState } from "react";
import Script from 'next/script';
import ReactImageMagnify from 'react-image-magnify';

import PageWrapper from "../../components/PageWrapper";
import Slider from "../../components/Slider";
import Head from "next/head";
import News from "../../components/Card";
import { Portal } from "@chakra-ui/portal";
import { getEngiinElectron, getEngiinBySlug, getOlonTariptBySlug, getOlonTaript, getEngiiinUsBySlug, getEngiinUs } from "../../../lib/api";
import EngiinSlider from "../../components/Slider/engiinUs";
const EngiinUs = ({ engiinusp, engiinus }) => {

 
  const Desktop = ({ children }) => {
    const isDesktop = useMediaQuery({ minWidth: 767 });
    return isDesktop ? children : null;
  };
  const Tablet = ({ children }) => {
    const isTablet = useMediaQuery({ minWidth: 768, maxWidth: 991 });
    return isTablet ? children : null;
  };
  const Mobile = ({ children }) => {
    const isMobile = useMediaQuery({ maxWidth: 767 });
    return isMobile ? children : null;
  };
  const Default = ({ children }) => {
    const isNotMobile = useMediaQuery({ minWidth: 768 });
    return isNotMobile ? children : null;
  };
  const Portrait = ({ children }) => {
    const isPortrait = useMediaQuery({ query: "(orientation: portrait)" });
    return isPortrait ? children : null;
  };

  const detail = [
    {
      title: "Tooluurin zagvar",
      value: "LXLC",
    },
    {
      title: "Hemjih hamgiin baga zaalt",
      value: "0.00000002M",
    },
    {
      title: "Temperaturin angi",
      value: "50C",
    },
    {
      title: "Tooluurin zagvar",
      value: "LXLC",
    },
    {
      title: "Tooluurin zagvar",
      value: "LXLC",
    },
    {
      title: "Tooluurin zagvar1",
      value: "LXLC",
    },
    {
      title: "Tooluurin zagvar",
      value: "LXLC",
    },
  ];
  const images = [

    "https://images.unsplash.com/photo-1521985068534-cec90327c93b?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80",
    "https://images.unsplash.com/photo-1513323813850-c7318e3efc71?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80",
    "https://cdn.sanity.io/images/xm2z006s/production/3895de80bb1b86fc8491ff3448fbe060ecce5702-3616x3489.jpg",
    "https://images.unsplash.com/photo-1612171709946-7fc1298a5a53?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=626&q=80",

  ];

  const [currentImage, setCurrentImage] = useState(engiinusp[0].image[0]);
  return (

    <div>

      <Head>
        <title>Measurement.mn</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />


      </Head>

      <Desktop>
        {" "}
        <>
        
          <div className="w-full">
            <img id="smartImgShadow" src="https://cdn.sanity.io/images/xm2z006s/production/0f33e3677209b7716990d1196483b32678d79a03-6140x2601.jpg" />
          </div>
          <div className="flex justify-center content-center" >
            <div >

            </div>
             
                  </div>
                  <div id="topMargin" className="mx-auto h-96 mb-40">
            <h1 className="text-sm text-blue-500 text-normal ml-2 mb-4">
              BUTEEGDEHUUN-USNII TOOLUUR-ENGIIIN TOOLUUR
            </h1>
            <div id="mainDiv" className=" gap-8 grid grid-cols-2">
              <div className="">
                {/* <img className="w-full  mb-2" id="mainImg" src={currentImage} alt="abc" /> */}
                <div style={{width:'full',height:'full'}}>
<ReactImageMagnify {...{
smallImage: {
  alt: 'Wristwatch by Ted Baker London',
  isFluidWidth: true,

src:currentImage,
},
largeImage: {
  src:currentImage,
  width: 1400,
  height: 1200
}
}} />
</div>
                <div id="imgGrid" className="grid lg:grid-cols-4 md:grid-cols-3 gap-1">
                  {engiinusp[0].image.map((engiinus) => {
                        //  {productp[0].images?.map((data) => {
                    return (
                      <img
                        className="lg:h-32 md:h-24"
                        onClick={() => setCurrentImage(engiinus)}
                        id="smallImg"
                        src={engiinus}
                        alt="abc"
                      />
                    );
                  })}
                </div>
              </div>
              <div className="flex flex-col justify-between">
                <div className="w-5/6 mx-auto">
                  <h1 className="text-2xl mb-3">DN 50 huiten usnii tooluur</h1>
                  {detail.map((data2) => {
                    return (
                      <p className="font-bold text-black text-base">
                        {data2.title}:<span className="font-light">{"  " + data2.value}</span>
                      </p>
                    );
                  })}
                </div>
                <iframe  className="w-full h-80" src="https://www.youtube.com/embed/Hq6fX_OH8iI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

              </div>
            </div>
          </div>
          <div className="container mb-40 ">
            <h1 className="product-title">БУСАД БҮТЭЭГДЭХҮҮН</h1>
            {/* <Flex> */}
            <EngiinSlider comps={engiinus} length={5} />
            {/* </Flex> */}
          </div>
          {/* CSS */}
          <style jsx>{`
            .zurag {
              max-width: 100%;
              -moz-transition: all 0.9s;
              -webkit-transition: all 0.9s;
              transition: all 0.9s;
            }
            #mainImg {
              border-radius: 30px;
              transition: transform 2s, filter 1.5s ease-in-out;
              transform-origin: center center;
           
            }
             #mainImg:hover {
              filter: brightness(100%);
              transform: scale(1.2);
              border-radius: 30px;
            }
            .product-title {
              color: #4a77fa;
              font-size: 18px;
              margin-left: 40px;
              text-align: center;
            }
            #topMargin {
              margin-top: 115px;
              height: fit-content;
              width: 79%;
            }
            #mainDiv {
              grid-template-columns: 1fr 1fr;
            }
            #mainImg {
              border-radius:20px;
              height: 470px;
              object-fit: cover;
              -webkit-transition: all 0.3s ease-out;
    -moz-transition: all 0.3s ease-out;
    -ms-transition: all 0.3s ease-out;
    -o-transition: all 0.3s ease-out;
    transition: all 0.3s ease-out;
            }
            #smallImg {
              object-fit: cover;
              border-radius:20px;
            }
            #slider {
              margin-bottom: 5rem;
            }
          `}</style>
        </>
      </Desktop>
      <Portrait>
        <>
          <div className="flex flex-col w-full mt-20 border pt-2">
            <h1 className="text-sm text-blue-500 text-normal ml-2 mb-3">
              BUTEEGDEHUUN-USNII TOOLUUR-ENGIIIN TOOLUUR
            </h1>
            <img className="mb-1 border-4 border-gray-500" src={currentImage} alt="abc" />
            <div className="grid grid-cols-4 gap-1">
              {images.map((data2) => {
                return (
                  <img
                    src={data2}
                    alt="abc"
                    className="h-24"
                    onClick={() => setCurrentImage(data2)}
                  />
                );
              })}
            </div>
          </div>
          <div className="mx-auto my-4 w-11/12"> 
            {detail.map((data2) => {
              return (
                <p className="font-bold text-black text-base">
                  {data2.title}:<span className="font-light">{"  " + data2.value}</span>
                </p>
              );
            })}
          </div>
          <iframe width="450" height="280" src="https://www.youtube.com/embed/Hq6fX_OH8iI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </>
      </Portrait>
    </div>
  );
};

export default EngiinUs;
export const getServerSideProps = async ({ params }) => {
	const engiinusp = await getEngiiinUsBySlug(params.slug);
  const engiinus = await getEngiinUs();
	
	return {
		props: {
			engiinusp,
      engiinus
		},
	};
};
