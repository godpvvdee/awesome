import React from "react";
import ReactDOM from "react-dom";
// eslint-disable-next-line
// import "swiper/css/bundle";
// import "./styles.css";
import PageWrapper from "../components/PageWrapper";
// import Slider2 from "../components/Slider/CardSlider2";
import GerchilgeeSlider from "../components/Slider/gerchilgeeSlider";
import {getAllGerchilgee } from "../../lib/api"

const IndexPage = (gerchilgee) => {
  return (
    <>
      <PageWrapper >
      <img className="zurag" src="https://cdn.sanity.io/images/xm2z006s/production/04b6fd4cbe0365627584359f37c11177d13cba1c-1184x332.jpg" />  
      <h1 className="news-title mt-5 mb-3 ">ТУСГАЙ ЗӨВШӨӨРӨЛ,ГЭРЧИЛГЭЭ</h1>

      <div className="gerchilgee-container  mb-20">
        <GerchilgeeSlider  gerchilgeeprop={gerchilgee} length={5} />
      </div>
      </PageWrapper>
      <style jsx>{`
      
.swiper {
  width: 100%;
  height: 100%;
}
.gerchilgee-container{
  margin-left:10%;
  width:80%;
}
.swiper-slide {
  text-align: center;
  font-size: 18px;
  background: #fff;

  /* Center slide text vertically */
  display: -webkit-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  -webkit-justify-content: center;
  justify-content: center;
  -webkit-box-align: center;
  -ms-flex-align: center;
  -webkit-align-items: center;
  align-items: center;
}

.swiper-slide img {
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
}

      hr.new5{
        border: 4px solid blue;
        border-radius: 2px;
      }
      .zurag{
          width:100%;
      }
      .news-title{
        font-family: 'Roboto', sans-serif;
        color:black;
        font-size: 20px;
        font-famoly:
        margin-left:40px;
        text-align:center;
      }
        .news-p{
            color:#585858;
            font-family:Roboto;
            font-size:18px;
            
        }
        
        
      `}</style>
    </>
    
  );
};
export default IndexPage;

export async function getStaticProps(context) {
  const gerchilgee = await getAllGerchilgee();
  return {
    props: {
      gerchilgee
     
    }, 
  }

}

