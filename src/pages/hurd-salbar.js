import BigImg from "../assets/image/jpg/ElectricTooluur.jpg";
import  Card  from "../components/Card/hudaldaa2";
import  Carousel  from "../components/Carousel";
// import delgvvrSlider from "../components/Slider/news-slider";
import MySlider from "../components/Swiper";
import Second from "../components/Swiper/second";
import {getAllNews } from "../../lib/api";
const Hurd = () => {
  return (
    <>
     <div className="salbar-bg w-full">

     </div>
      <div className="grid grid-cols-1  md:grid-cols-2">    
      <MySlider/>
      <Second/>
      </div>

      <div className="  w-full flex flex-col items-center  bg-white pt-8">

      <h1 className="text-blue-500 text-lg border-b-2 mt-5 mb-5 border-blue-500 text-center">Хэмжих хэрэгсэлийн дэлгүүр</h1>
   
<div className="container">
What is Lorem Ipsum?
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

Why do we use it?
It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
       
</div>
        <div className="mt-5">
        <h1 className="text-blue-500 text-lg border-b-2 mt-5 mb-5 border-blue-500 text-center"></h1>

          <div className="grid grid-cols-1  md:grid-cols-2">
            
            <div className="block items-end ">
              <iframe width="450px" height="500px" src="https://momento360.com/e/u/21672faefa46484fa68dcb602ce427b6?utm_campaign=embed&utm_source=other&heading=-1.2&pitch=-31.1&field-of-view=75&size=medium
" frameborder="0"></iframe>
              <div className="my-2  md:w-10/12 mx-2">
                <h1 className="text-base text-blue-500">ДЭЛГҮҮР-1</h1>
                <p className="text-sm max-w-md text-gray-500 font-medium">
                Монгол улс, Улаанбаатар хот, Баянзүрх дүүрэг, 4-р хороо, 15-р хороолол, 49-0

                </p>
              </div>
            </div>
            <div className="block">
            <iframe width="450px" height="500px" src="https://momento360.com/e/u/5f24534539bd49969b5f66f33c1a2cac?utm_campaign=embed&utm_source=other&heading=28.4&pitch=-7.5&field-of-view=75&size=medium" frameborder="0"></iframe>

              <div className="my-2 ml-1">
                <h1 className="text-base  md:w-10/12 text-blue-500">ДЭЛГҮҮР-2,ЛАБОРАТОРИ</h1>
                <p className="text-sm max-w-md text-gray-500 font-medium">
                  Монгол улс, Улаанбаатар хот, Хан-Уул дүүргийн 15-р хороо ХУРД Рапид харш хороолол,21-р байрны 73 тоот
                </p>
              </div>
            </div>
          </div>
          <h1 className="text-blue-500 text-lg border-b-2 mt-5 mb-5 border-blue-500 text-center"></h1>

        </div>

      </div>
      <style jsx>
        {`
        .salbar-bg{
          height:100px;
          background: linear-gradient( 
            45deg
            ,rgba(45,133,192,1) 0%,rgba(18,87,151,1) 24%,rgba(35,115,176,1) 24%,rgba(17,119,175,1) 61%,rgba(33,139,193,1) 61%,rgba(88,204,255,1) 100% );
        }
          #smartImgShadow {
            -webkit-box-shadow: 0 6px 7px -6px black;
            -moz-box-shadow: 0 6px 7px -6px black;
            box-shadow: 0 6px 7px -6px black;
          }
        `}
      </style>
    </>
  );
};

export default Hurd;
export async function getStaticProps(context) {

  const medee = await getAllNews();

  return {
    props: { 
      medee:medee[0],
    }, 
  }

}
