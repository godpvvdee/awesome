import React, { Component } from 'react';
import Head from "next/head";
import Sidebar from "../components/Sidebar/Sidebar";
import Card from "../components/Card/uhaalagTooluur"
import Slider from "../components/Slider";
import { Container,Row,Col} from 'react-bootstrap';
import { Tabs, TabList, TabPanels, Tab, TabPanel } from '@chakra-ui/react'
import { getAllProduct } from "../../lib/api";
import VerticalTabs from '../components/Tab';

const Smart = (data) => {
 console.log("uhaalagTooluurr",data);
 var data2 = data.data;
 console.log(typeof data2);
 console.log("this is ",data2);
 
  return (
    <>
    <Head>
    
        <title>Measurement.mn</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
   

      <div className="w-full">
        <img id="smartImgShadow" src= "https://cdn.sanity.io/images/xm2z006s/production/0f33e3677209b7716990d1196483b32678d79a03-6140x2601.jpg"/>
      </div>
   
      <VerticalTabs data={data2} ></VerticalTabs>




      <style jsx>
        {`
          #smartImgShadow {
            -webkit-box-shadow: 0 6px 7px -6px black;
            -moz-box-shadow: 0 6px 7px -6px black;
            box-shadow: 0 6px 7px -6px black;
          }
        `}
      </style>
    </>
  );
};

export default Smart;


export const getServerSideProps = async({params})=> {
  const data = await getAllProduct();

  return {
    props: {
      data,
     
    }, 
  }

}