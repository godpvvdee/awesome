import BigImg from "../assets/image/jpg/ElectricTooluur.jpg";

const Software = () => {
  return (
    <>
      <div className="w-full">
        <img id="smartImgShadow" src={BigImg} />
      </div>
      <div className="  w-full flex flex-col items-center bg-white pt-8">
          <h1 className="text-blue-500 text-lg border-b-2 border-blue-500 text-center">
            Програм хангамж
          </h1>
          <iframe width="600px" height="600px" src="https://momento360.com/e/u/5f24534539bd49969b5f66f33c1a2cac?utm_campaign=embed&utm_source=other&heading=28.4&pitch=-7.5&field-of-view=75&size=medium" frameborder="0"></iframe>
          <iframe width="600px" height="600px" src="          https://momento360.com/e/u/21672faefa46484fa68dcb602ce427b6?utm_campaign=embed&utm_source=other&heading=-1.2&pitch=-31.1&field-of-view=75&size=medium
" frameborder="0"></iframe>

        </div>
      <style jsx>
        {`
          #smartImgShadow {
            -webkit-box-shadow: 0 6px 7px -6px black;
            -moz-box-shadow: 0 6px 7px -6px black;
            box-shadow: 0 6px 7px -6px black;
          }
        `}
      </style>
    </>
  );
};

export default Software;
