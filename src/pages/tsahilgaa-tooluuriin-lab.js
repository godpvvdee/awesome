import React, { Component } from 'react';
import Head from "next/head";
import {  getJinZaswar, getTsahilgaanLab, getUslab } from "../../lib/api";
import VerticalTabs from '../components/Tab/zaswarVilchilgee';

const tsahilgaanLab = (tsahilgaanTooluurLab,usLab,jinZaswar) => {
 var tsahilgaanTooluurLab2 = tsahilgaanTooluurLab.tsahilgaanTooluurLab;
var usLab2 = tsahilgaanTooluurLab.usLab;
var jinZaswar2 = tsahilgaanTooluurLab.jinZaswar;
  return (
    <>
    <Head>
        <title>Measurement.mn</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div className="w-full">
        <img id="smartImgShadow" src= "https://cdn.sanity.io/images/xm2z006s/production/0f33e3677209b7716990d1196483b32678d79a03-6140x2601.jpg"/>
      </div>
      <VerticalTabs tsahilgaanTooluurLab={tsahilgaanTooluurLab2} usLab={usLab2} jinZaswar={jinZaswar2}></VerticalTabs>
      <style jsx>
        {`
          #smartImgShadow {
            -webkit-box-shadow: 0 6px 7px -6px black;
            -moz-box-shadow: 0 6px 7px -6px black;
            box-shadow: 0 6px 7px -6px black;
          }
        `}
      </style>
    </>
  );
};

export default tsahilgaanLab;


export const getServerSideProps = async({params})=> {
  const tsahilgaanTooluurLab = await getTsahilgaanLab();
const usLab = await getUslab();
const jinZaswar = await getJinZaswar();
  return {
    props: {
      tsahilgaanTooluurLab,    
      usLab, 
      jinZaswar,
    }, 
  }

}