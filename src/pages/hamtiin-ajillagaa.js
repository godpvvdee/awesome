import BigImg from "../assets/image/jpg/ElectricTooluur.jpg";
import Partners from "../components/Slider/PartnerSlider";
import Head from "next/head";
const company = [
    "https://cdn.sanity.io/images/xm2z006s/production/986880f990ed116e66e3eece819eb9011292c20a-142x148.jpg",
    "https://cdn.sanity.io/images/xm2z006s/production/e82f16f3887a95c9931d189d8cc71e4132a617f6-142x148.jpg",
    "https://cdn.sanity.io/images/xm2z006s/production/c97573fe398170f297eff0c90d9b306fd56934b8-142x148.jpg",
    "https://cdn.sanity.io/images/xm2z006s/production/884feae3a4aeff3af5f4afa77dd3887a115de89c-142x148.jpg",
    "https://cdn.sanity.io/images/xm2z006s/production/7cde1b64926d12b0f7f225c73cd83094846b58f5-142x148.jpg",
    "https://cdn.sanity.io/images/xm2z006s/production/ebbbd99a846c6bc9537c91ef12b9f9aac0e116a3-142x148.jpg",
    "https://cdn.sanity.io/images/xm2z006s/production/1a567e21279e5908a32ad4652382e10f013faafd-142x148.jpg",
    "http://amjiltacademy.com/wp-content/uploads/2021/12/tom-amjilt-logo.png",
    "https://cdn.sanity.io/images/xm2z006s/production/a61ecbff24926c1cc0be7220265ccda8aa7510da-142x148.jpg",
    "https://cdn.sanity.io/images/xm2z006s/production/76ea451cb2e6e01dede4eb765eab5ba43ad3e76d-142x148.jpg",
  
  "https://cdn.sanity.io/images/xm2z006s/production/c4782d39a5eef24941f9ac22bbfd970e30ee8126-142x148.jpg",
  "https://cdn.sanity.io/images/xm2z006s/production/91c622725dc45ccdf8b126d46a8139651f727fe3-142x148.jpg",
  "https://cdn.sanity.io/images/xm2z006s/production/c092a2cca8e1daf8aad30ae17b8bb03593caf83d-142x148.jpg",
  "https://cdn.sanity.io/images/xm2z006s/production/4da2450de932538a93d392e78b73c1e84d4f40f9-142x148.jpg",
  "https://cdn.sanity.io/images/xm2z006s/production/2689bd2d746586e41bcf21fe99dc5ac11f2129e9-142x148.jpg",
  "https://cdn.sanity.io/images/xm2z006s/production/0f3d680ad0751c2da31b14947a26ae866472eeea-142x148.jpg",
  "https://cdn.sanity.io/images/xm2z006s/production/82891e9ea730655eaa1e270a3923c5461da63a66-142x148.jpg",
  "https://cdn.sanity.io/images/xm2z006s/production/76dc7cb872aaf93999daba11eda1cf23fa68d652-142x148.jpg",
  "https://cdn.sanity.io/images/xm2z006s/production/68379424a0a9f222a52de7ce8956cd6723f90faa-142x148.jpg"
  ];
const Index = () => {
  return (
    <>
    <Head>
    <title>Measurement.mn</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div className="w-full">
        <img id="smartImgShadow" src= "https://cdn.sanity.io/images/xm2z006s/production/0f33e3677209b7716990d1196483b32678d79a03-6140x2601.jpg"/>
      </div>
      <div className="  w-full flex flex-col items-center bg-white pt-8">
          <h1 className="text-blue-500 text-lg border-b-2 border-blue-500 text-center">
            ХАМТЫН АЖИЛЛАГАА.
          </h1>
          <Partners comps={company} />
        </div>
      <style jsx>
        {`
          #smartImgShadow {
            -webkit-box-shadow: 0 6px 7px -6px black;
            -moz-box-shadow: 0 6px 7px -6px black;
            box-shadow: 0 6px 7px -6px black;
          }
        `}
      </style>
    </>
  );
};

export default Index;
