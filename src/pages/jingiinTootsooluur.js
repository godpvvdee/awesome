import React, { Component } from 'react';
import Head from "next/head";

import { getDatchik, getJinTO } from "../../lib/api";
import TabElec from '../components/Tab/electronJin';

const Electron = (jingiinTootsooluur,datchik) => {
 
var jingiinTootsooluur2 = jingiinTootsooluur.jingiinTootsooluur;
var datchik2 = jingiinTootsooluur.datchik;
console.log('psda',datchik2);
console.log('main-jin-tootsooluur',jingiinTootsooluur2);

  return (
    <>
    <Head>
        <title>Measurement.mn</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div className="w-full">
        <img id="smartImgShadow" src= "https://cdn.sanity.io/images/xm2z006s/production/0f33e3677209b7716990d1196483b32678d79a03-6140x2601.jpg"/>
      </div>
      <TabElec  jingiinTootsooluur={jingiinTootsooluur2} datchik={datchik2} ></TabElec>
      <style jsx>
        {`
          #smartImgShadow {
            -webkit-box-shadow: 0 6px 7px -6px black;
            -moz-box-shadow: 0 6px 7px -6px black;
            box-shadow: 0 6px 7px -6px black;
          }
        `}
      </style>
    </>
  );
};

export default Electron;


export const getServerSideProps = async({params})=> {
const jingiinTootsooluur = await getJinTO();
const datchik  = await getDatchik();
  return {
    props: {
     
      jingiinTootsooluur,
      datchik,
    }, 
  }

}