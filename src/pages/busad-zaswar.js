import React, { Component } from 'react';
import Head from "next/head";
import Sidebar from "../components/Sidebar/Sidebar";
import Card from "../components/Card/uhaalagTooluur"
import { Container,Row,Col} from 'react-bootstrap';
import { Tabs, TabList, TabPanels, Tab, TabPanel } from '@chakra-ui/react'
import { getAllProduct, getBusad, getEngiinElectron } from "../../lib/api";
import VerticalTabs from '../components/Tab';
import Slider from '../components/Slider/busadZaswar';
const Smart = (busad) => {
 var busad2 = busad.busad;
 console.log('busad',busad2);

  return (
    <>
    <Head>
    
        <title>Measurement.mn</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
   

      <div className="w-full">
        <img id="smartImgShadow" src= "https://cdn.sanity.io/images/xm2z006s/production/0f33e3677209b7716990d1196483b32678d79a03-6140x2601.jpg"/>
      </div>
   
      <h1 className="product-title">БҮТЭЭГДЭХҮҮН.</h1>

      <Slider comps={busad2} length={5} />



      <style jsx>
        {`
        .product-title {
          text-align:center;
          color: #4a77fa;
          font-size: 18px;
          margin-left: 40px;
        }
          #smartImgShadow {
            -webkit-box-shadow: 0 6px 7px -6px black;
            -moz-box-shadow: 0 6px 7px -6px black;
            box-shadow: 0 6px 7px -6px black;
          }
        `}
      </style>
    </>
  );
};

export default Smart;



export const getServerSideProps = async({params})=> {
  const busad = await getBusad();
 
  return {
    props: {
      busad,
      
     
     
    }, 
  }

}