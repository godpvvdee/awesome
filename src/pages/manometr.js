import React, { Component } from 'react';
import Head from "next/head";
import Sidebar from "../components/Sidebar/Sidebar";
import Card from "../components/Card/uhaalagTooluur"
import Slider from "../components/Slider";
import { Container,Row,Col} from 'react-bootstrap';
import { Tabs, TabList, TabPanels, Tab, TabPanel } from '@chakra-ui/react'
import { getAllProduct, getCollector, getConcentrator, getDaralt, getEngiinElectron, getManometr, getOlonTaript } from "../../lib/api";
import ManometrTabs from '../components/Tab/dulaanDaralt';

const Manometr = (manometr,daralt) => {
 var manometr2 = manometr.manometr;
 console.log('manometr',daralt);
 var  daralt2 =  manometr.daralt;
 

  return (
    <>
    <Head>
        <title>Measurement.mn</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div className="w-full">
        <img id="smartImgShadow" src= "https://cdn.sanity.io/images/xm2z006s/production/0f33e3677209b7716990d1196483b32678d79a03-6140x2601.jpg"/>
      </div>
      <ManometrTabs manometr={manometr2} daralt={daralt2} ></ManometrTabs>
      <style jsx>
        {`
          #smartImgShadow {
            -webkit-box-shadow: 0 6px 7px -6px black;
            -moz-box-shadow: 0 6px 7px -6px black;
            box-shadow: 0 6px 7px -6px black;
          }
        `}
      </style>
    </>
  );
};

export default Manometr;


export const getServerSideProps = async({params})=> {
  const manometr = await getManometr();
  const daralt =  await getDaralt();

  return {
    props: {
      manometr,
      daralt,
     
     
    }, 
  }

}